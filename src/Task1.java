import model.Book;

import java.util.Scanner;


/**
 * Created by Максим on 14.03.2017.
 */
public class Task1 {

    public static void main(String[] args) {

        Book [] books = new Book[5];

        books [0] = new Book("War And Peace","Lev Tolstoy",1869);

        books [1] = new Book("11/22/63","Stiven King",2011);

        books [2] = new Book("Romeo and Juliet","William Shakespeare",1597);

        books [3] = new Book("The Hound Of The Baskervilles","Conan Doyle",1902);

        books [4] = new Book("The Martian", "Andy Weir",2011);

        Book olderYearBook = books[0];
        // TheOlderBook
        for (int i = 0; i < 5 ; i++) {
            if ( olderYearBook.getYear() > books[i].getYear() ) {
                olderYearBook = books[i];
            }
        }
        System.out.println( "The older book: " + olderYearBook.getTitle());
        System.out.println( "Author: " + olderYearBook.getAuthor());

        // findAuthorWithHisBooks
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter author: ");
        String author = scan.nextLine();
        boolean d = false;
        for (Book book : books) {
            if (author.equalsIgnoreCase(book.getAuthor())){
                System.out.println(book.getTitle());
                d = true;
            }
        }
        if (!d) {
            System.out.println("This author has not books.");
        }

        // isOlderThen
        Scanner scan1 = new Scanner(System.in);
        System.out.println("Find books older then ");
        int year = scan1.nextInt();
        scan1.nextLine();
        boolean d1 = false;
        for (Book book : books) {
            if (year > book.getYear()){
                System.out.println("Book: " + book.getTitle() + " Author: " + book.getAuthor() + " Year publisher: " + book.getYear());
                d1 = true;
            }
        }
        if (!d1) {
            System.out.println("Do not find this books!s");
        }
        }
    }

